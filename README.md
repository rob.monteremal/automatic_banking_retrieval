<a name="readme-top"></a>

<!-- TABLE OF CONTENTS -->
<details>
    <summary>Table of Contents</summary>
    <ol>
    <li>
        <a href="#about-the-project">About The Project</a>
    </li>
    <li>
        <a href="#getting-started">Getting Started</a>
        <ul>
            <li><a href="#prerequisites">Prerequisites</a></li>
            <li><a href="#installation">Installation</a></li>
        </ul>
    </li>
    <li>
        <a href="#usage">Usage</a>
        <ul>
            <li><a href="#first-run">First run</a></li>
            <li><a href="#documentation">Documentation</a></li>
            <li><a href="#using-the-spreadsheet">Using the spreadsheet</a></li>
        </ul>
    </li>
    <li><a href="#roadmap">Roadmap</a></li>
    <li><a href="#acknowledgments">Acknowledgments</a></li>
    </ol>
</details>

<!-- ABOUT THE PROJECT -->

## About The Project

Retrieve your bank transactions and account balance using Python functions interfacing with GoCardless (formerly Nordigen) Bank Account Data API.

**Note:** The GoCardless API is built around the [PSD2 protocol](https://en.wikipedia.org/wiki/Payment_Services_Directive) which can only be used for payment-related transactions, therefore you will not be able to retrieve your savings and investments account balance or transactions.

### Features

- Full GoCardless authentication integration
- Retrieve your bank account balance
- Retrieve your last x days worth of transactions
- Automatically import your new transactions in your [Expense tracking spreadsheet](https://gitlab.com/rob.monteremal/expense_tracking_template)

<!-- GETTING STARTED -->

## Getting Started

This project is built around GoCardless Bank Account Data API, you will need to [create a GoCardless Bank Account Data API account](https://bankaccountdata.gocardless.com/overview/) in order to use it.

### Prerequisites

- [Python 3.10](https://apps.microsoft.com/detail/python-3-10/9PJPW5LDXLZ5?hl=en-US&gl=US)
- pip

If you want to use the spreadsheet-related functionnalities:

- Windows
- Microsoft Excel 365

### Installation

1. Log in to your [GoCardless Bank Account Data API portal](https://bankaccountdata.gocardless.com/overview/)
2. Create a new [User ID/User secret combo](https://bankaccountdata.gocardless.com/user-secrets/), leave the default IP mask
3. Clone the repo
   ```sh
   git clone https://gitlab.com/rob.monteremal/automatic_banking_retrieval.git
   cd automatic_banking_retrieval
   ```
4. Install Python dependencies
   ```sh
   pip install -r requirements.txt
   ```
5. Rename all the template JSON files in the `auth` folder to remove the _.template_ suffix from the file names
6. Set your User ID/User secret in `auth/secrets.json`
   ```json
   {
     "SECRET_ID": "<SECRET_ID>",
     "SECRET_KEY": "<SECRET_KEY>"
   }
   ```
7. Set your bank identifier list in `auth/bank_accounts.json`.
   ```json
   {
     "bank_id": "<BANK_IDENTIFIER>",
     "logged": false
   }
   ```
   In order to get the indentifier for a particular bank, either look through the already existing `bank_list_FR.txt` for French ones, or run the `get_banks(<COUNTRY_CODE>)` function in `gocardless_account.py`

<!-- USAGE EXAMPLES -->

## Usage

### First run

1. Follow the steps in the [Installation section](#installation)
2. Run the `get_all_accounts()` from the `gocardless_account.py` file
3. An authentication link should be outputted to your terminal, open it in your web browser
4. Authenticate with your bank account credentials and any 2-step verification if necessary
5. Wait for up to a minute for the redirect to happen, **you should be redirected to a timeout/error page**
6. Copy the redirect URL (should contain the keyword "ref") from your web browser address bar and paste it in your terminal
7. Repeat steps 3 to 6 for each of the banks that you added in the `auth/bank_accounts.json` file
8. You should be properly authenticated and can proceed to using the balance and transaction functions, you can verify it by running the `gocardless_balance.py` script and you should see your first bank account balance appear

This process needs to be repeated every 180 days, which is the maximum validity duration for GoCardless bank authentication.

### Documentation

In order to use most functions, you need to provide a bank account ID. You can either run the `get_all_accounts()` function to get the list of all of your bank account IDs, or run `get_accounts()` and providing it with a corresponding bank ref ID found in the `auth/bank_accounts.json` after authenticating said bank.

- `gocardless_account.py` contains all functions related to account authentication and account information
- `gocardless_transactions.py` for fetching your recent bank transactions
- `gocardless_balance.py` for getting your account balance

### Using the spreadsheet

In order to use the spreadsheet functionnalities, you need to download and set up the [Expense tracking spreadsheet](https://gitlab.com/rob.monteremal/expense_tracking_template), you can follow the instructions over there.

Afterwards, just run the following command to automagically import your bank transactions:

```sh
python3 spreadsheet.py <PATH_TO_SPREADSHEET>
```

By default the last 30 days worth of transactions will be retrieved. If you have already manually inputted some transactions to the spreadsheet during this period, there may be some duplicates. You'll need to manually delete them from the spreadsheet and make sure that the ID of the duplicate transactions is properly set in the ID column of the spreadsheet.

### Expiry
If and when your GoCardless authentication expires, you will need to reauthenticate. You can do so by first setting  `"logged": false` for every account that needs to be reauthenticated in `auth/bank_accounts.json` and *then* running the `get_all_accounts()` function again and following the steps in the [First run section](#first-run).

<!-- ROADMAP -->

## Roadmap

- [x] Add a safeguard against overwriting transactions
- [x] Use balance in spreadsheet to detect descrepencies between running total and actual balance
- [ ] Properly tag internal transactions for Excel categorization (using bankTransactionCode when available)
- [ ] Generate matching/opposite internal transaction when possible
- [x] Find a way to import transactions and balance from other accounts
- [ ] Automatically re-authenticate when necessary

<!-- ACKNOWLEDGMENTS -->

## Acknowledgments

- [GoCardless (formerly Nordigen)](https://gocardless.com/)
- [xlwings](https://docs.xlwings.org/en/0.30.2/index.html)
- [Best README Template](https://github.com/othneildrew/Best-README-Template)
