import json
from gocardless_token import get_access_token
from tls_client import Session

ACCOUNTS_FILE_PATH = "auth/bank_accounts.json"


def create_end_user_agreement(inst_id: str) -> str:
    sesh = Session(client_identifier="chrome_115", random_tls_extension_order=True)
    url = "https://bankaccountdata.gocardless.com/api/v2/agreements/enduser/"
    headers = {
        "Accept": "application/json",
        "Content-Type": "application/json",
        "Authorization": f"Bearer {get_access_token()}",
    }

    r = sesh.post(
        url,
        headers=headers,
        json={
            "institution_id": inst_id,
            "max_historical_days": 30,
            "access_valid_for_days": 180,
            "access_scope": ["balances", "transactions"],
        },
    )
    if 200 <= r.status_code < 300:
        return r.json()["id"]
    else:
        print(r.text)
        raise Exception("Failed to create end user agreement")


def generate_auth_link(eua_id: str, inst_id: str) -> str:
    sesh = Session(client_identifier="chrome_115", random_tls_extension_order=True)
    url = "https://bankaccountdata.gocardless.com/api/v2/requisitions/"
    headers = {
        "Accept": "application/json",
        "Content-Type": "application/json",
        "Authorization": f"Bearer {get_access_token()}",
    }

    r = sesh.post(
        url,
        headers=headers,
        json={
            "redirect": "http://localhost:8000/",
            "institution_id": inst_id,
            "agreement": eua_id,
            "user_language": "EN",
        },
    )
    if 200 <= r.status_code < 300:
        return r.json()["link"]
    else:
        print(r.text)
        raise Exception("Failed to generate registration link")


def register_bank(inst_id: str) -> str:
    eua_id = create_end_user_agreement(inst_id)
    auth_url = generate_auth_link(eua_id, inst_id)
    print(
        f"Authentication link (CTRL+click to open in your web browser and log in with your bank): {auth_url}"
    )
    redirect_url = input("Paste the URL you were redirected to: ")
    return redirect_url.split("?ref=")[1]


def register_all_banks() -> list[str]:
    with open(ACCOUNTS_FILE_PATH, "r+") as bank_accounts_file:
        bank_accounts = json.load(bank_accounts_file)
        updated = False
        for bank in bank_accounts:
            if bank.get("logged", False) != True:
                bank["ref"] = register_bank(bank["bank_id"])
                bank["logged"] = True
                updated = True
        if updated:
            bank_accounts_file.seek(0)  # rewind
            json.dump(bank_accounts, bank_accounts_file)
            bank_accounts_file.truncate()
        return [bank["ref"] for bank in bank_accounts]


def get_account_info(account_id: str):
    sesh = Session(client_identifier="chrome_115", random_tls_extension_order=True)
    url = f"https://bankaccountdata.gocardless.com/api/v2/accounts/{account_id}/"
    headers = {
        "Accept": "application/json",
        "Authorization": f"Bearer {get_access_token()}",
    }

    r = sesh.get(url, headers=headers)
    if 200 <= r.status_code < 300:
        return r.json()
    else:
        print(r.text)
        raise Exception("Failed to get account info")


def get_accounts(eua_id: str):
    sesh = Session(client_identifier="chrome_115", random_tls_extension_order=True)
    url = f"https://bankaccountdata.gocardless.com/api/v2/requisitions/{eua_id}/"
    headers = {
        "Accept": "application/json",
        "Authorization": f"Bearer {get_access_token()}",
    }

    r = sesh.get(url, headers=headers)
    if 200 <= r.status_code < 300:
        return r.json()["accounts"]
    else:
        print(r.text)
        raise Exception("Failed to get accounts")


def get_all_accounts() -> list[str]:
    return [account for ref in register_all_banks() for account in get_accounts(ref)]


def get_banks(country_code: str = "FR"):
    sesh = Session(client_identifier="chrome_115", random_tls_extension_order=True)
    url = "https://bankaccountdata.gocardless.com/api/v2/institutions/"
    headers = {
        "Accept": "application/json",
        "Authorization": f"Bearer {get_access_token()}",
    }

    r = sesh.get(url, headers=headers, params={"country": country_code})
    if 200 <= r.status_code < 300:
        institutions = r.json()
        return "\n".join([f"{inst['name']}({inst['id']})" for inst in institutions])
    else:
        print(r.text)
        raise Exception("Failed to get institutions")


if __name__ == "__main__":
	print(get_all_accounts())
    # print(get_account_info(get_all_accounts()[0]))
    # print(get_banks())
