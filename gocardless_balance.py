from gocardless_token import get_access_token
from gocardless_account import get_all_accounts
from tls_client import Session


def get_balances(account_id: str):
    sesh = Session(client_identifier="chrome_115", random_tls_extension_order=True)
    url = (
        f"https://bankaccountdata.gocardless.com/api/v2/accounts/{account_id}/balances/"
    )
    headers = {
        "Accept": "application/json",
        "Authorization": f"Bearer {get_access_token()}",
    }

    r = sesh.get(url, headers=headers)
    if 200 <= r.status_code < 300:
        return r.json()["balances"]
    else:
        print(r.text)
        raise Exception("Failed to get balance")


def get_default_balance(account_id: str) -> float:
    return float(get_balances(account_id)[0]["balanceAmount"]["amount"])


if __name__ == "__main__":
    for account in get_all_accounts():
        print(account)
        print(get_balances(account))
