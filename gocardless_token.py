import json, time
from tls_client import Session

TOKEN_FILE_PATH = "auth/token.json"
SECRETS_FILE_PATH = "auth/secrets.json"


def get_access_token() -> str:
    with open(TOKEN_FILE_PATH, "r") as token_file:
        token = json.load(token_file)
        if token["access_expires"] > time.time():
            return token["access"]
        elif token["refresh_expires"] > time.time():
            return refresh_access_token(token)
        else:
            return get_refresh_token()


def refresh_access_token(token: dict) -> str:
    sesh = Session(client_identifier="chrome_115", random_tls_extension_order=True)
    url = "https://bankaccountdata.gocardless.com/api/v2/token/refresh/"
    headers = {"Accept": "application/json", "Content-Type": "application/json"}

    r = sesh.post(url, headers=headers, json={"refresh": token["refresh"]})
    if r.status_code == 200:
        return write_access_token(r.json())
    else:
        raise Exception("Failed to refresh token")


def get_refresh_token() -> str:
    with open(SECRETS_FILE_PATH, "r") as secrets_file:
        secrets = json.load(secrets_file)
        sesh = Session(client_identifier="chrome_115", random_tls_extension_order=True)
        url = "https://bankaccountdata.gocardless.com/api/v2/token/new/"
        headers = {"Accept": "application/json", "Content-Type": "application/json"}

        r = sesh.post(
            url,
            headers=headers,
            json={
                "secret_id": secrets["SECRET_ID"],
                "secret_key": secrets["SECRET_KEY"],
            },
        )
        if r.status_code == 200:
            return write_refresh_token(r.json())
        else:
            raise Exception("Failed to get refresh token")


def write_refresh_token(token: dict) -> str:
    with open(TOKEN_FILE_PATH, "w") as token_file:
        token["access_expires"] = time.time() + token["access_expires"]
        token["refresh_expires"] = time.time() + token["refresh_expires"]
        json.dump(token, token_file)
        return token["access"]


def write_access_token(new_token: dict) -> str:
    with open(TOKEN_FILE_PATH, "r+") as token_file:
        token = json.load(token_file)
        token["access"] = new_token["access"]
        token["access_expires"] = time.time() + new_token["access_expires"]

        token_file.seek(0)  # rewind
        json.dump(token, token_file)
        token_file.truncate()

        return token["access"]


if __name__ == "__main__":
    print(get_access_token())
