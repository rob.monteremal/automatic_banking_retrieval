from datetime import date


class TransactionRow:
    def __init__(self, obj: dict):
        self.id = obj["id"]
        self.operation_date = obj["operation_date"]
        self.value_date = obj["value_date"]
        self.account = obj["account"]
        self.label = obj["label"]
        self.comment = obj["comment"]
        self.amount = obj["amount"]

    id: str
    operation_date: date
    value_date: date
    account: str
    label: str
    comment: str
    amount: float
