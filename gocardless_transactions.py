import datetime
from gocardless_token import get_access_token
from gocardless_account import get_all_accounts, get_account_info
from tls_client import Session
from _types import TransactionRow


def get_transactions(account_id: str):
    sesh = Session(client_identifier="chrome_115", random_tls_extension_order=True)
    url = f"https://bankaccountdata.gocardless.com/api/v2/accounts/{account_id}/transactions/?date_from=2024-01-01"
    headers = {
        "Accept": "application/json",
        "Authorization": f"Bearer {get_access_token()}",
    }

    r = sesh.get(url, headers=headers)
    if 200 <= r.status_code < 300:
        return r.json()["transactions"]
    else:
        print(r.text)
        raise Exception("Failed to get transactions")


def get_transaction_rows(account_id: str):
    account = get_account_info(account_id)
    return [
        map_transaction(transaction, account["institution_id"], account["iban"])
        for transaction in get_transactions(account_id)["booked"]
    ]


def read_date(date_str: str) -> datetime.date:
    return datetime.date.fromisoformat(date_str)


def map_transaction(transaction: dict, bank_id: str, iban: str) -> TransactionRow:
    amount = float(transaction["transactionAmount"]["amount"])

    # Fortuneo
    if bank_id == "FORTUNEO_FTNOFRP1XXX":
        labels: list[str] = transaction["remittanceInformationUnstructuredArray"][
            0
        ].split("  ")
        card_date = (
            datetime.datetime.strptime(
                f"{labels[0][6:11]}/{str(datetime.datetime.today().year)}", "%d/%m/%Y"
            ).date()
            if labels[0].startswith("CARTE")
            else None
        )
        return TransactionRow(
            {
                "id": transaction["internalTransactionId"],
                "operation_date": (
                    card_date if card_date else read_date(transaction["bookingDate"])
                ),
                "value_date": read_date(transaction["valueDate"]),
                "account": iban,
                "label": labels.pop(0) if len(labels) > 0 else "",
                "comment": labels.pop(0) if len(labels) > 0 else "",
                "amount": amount,
            }
        )
    # Caisse d'Epargne LDA
    elif bank_id == "CAISSEDEPARGNE_LOIRE_DROME_ARDECHE_CEPAFRPP426":
        return TransactionRow(
            {
                "id": transaction["internalTransactionId"],
                "operation_date": read_date(transaction["bookingDate"]),
                "value_date": read_date(transaction["bookingDate"]),
                "account": iban,
                "label": transaction["remittanceInformationUnstructuredArray"][0],
                "comment": "",
                "amount": amount,
            }
        )
    # Swan (provider for other online banks)
    elif bank_id == "SWAN_SWNBFR22":
        return TransactionRow(
            {
                "id": transaction["transactionId"].split("_")[1],
                "operation_date": read_date(transaction["bookingDate"]),
                "value_date": read_date(transaction["valueDate"]),
                "account": iban,
                "label": transaction["debtorName" if amount > 0 else "creditorName"],
                "comment": "",
                "amount": amount,
            }
        )


if __name__ == "__main__":
    print(get_transaction_rows(get_all_accounts()[1]))
