import xlwings as xl
import sys
from shutil import copy
from _types import TransactionRow
from gocardless_transactions import get_transaction_rows
from gocardless_account import get_all_accounts, get_account_info
from gocardless_balance import get_default_balance

SPREADSHEET_PATH = "test.xlsx"
MAX_ROWS = 5000  # Max number of rows in the spreadsheet to increase performance


def map_transaction_row_spreadsheet(transaction: TransactionRow, name_iban_map: dict):
    amount = transaction.amount
    return [
        transaction.id,
        transaction.operation_date,
        transaction.value_date,
        name_iban_map.get(transaction.account, None),
        transaction.label,
        transaction.comment,
        None,
        None,
        amount if (amount < 0) else None,
        amount if (amount > 0) else None,
    ]


def write_transactions(
    wb: xl.Book, transaction_rows: list[TransactionRow], name_iban_map: dict, last_row
):
    transactions = [
        map_transaction_row_spreadsheet(transaction, name_iban_map)
        for transaction in transaction_rows
    ]
    sheet = wb.sheets["Opérations"]
    filter_duplicate_keys(sheet, "A", 0, transactions)
    if len(transactions) > 0:
        write_rows(sheet, last_row, 0, transactions)
    else:
        print("No new transactions to write")
    return last_row + len(transactions)


def write_all_transactions(wb: xl.Book):
    accounts = get_all_accounts()
    name_iban_map = get_account_name_iban_map(wb, accounts)
    sheet = wb.sheets["Opérations"]
    last_row = find_last_row(sheet, "A")
    print(f"Last row: {last_row}")
    for account_id in accounts:
        last_row = write_transactions(
            wb, get_transaction_rows(account_id), name_iban_map, last_row
        )
    balance_iban_map = get_account_balance_iban_map(wb, accounts)
    
    errors = []
    for account_id in accounts:
        if (api_bal := get_default_balance(account_id)) != (
            sheet_bal := balance_iban_map[get_account_info(account_id)["iban"]]
        ):
            errors.append(
                f"Balance mismatch for account {account_id}: API: {api_bal}€ != Excel: {sheet_bal}€"
            )
    if len(errors) > 0:
        raise Exception("\n".join(errors))


def get_account_name_iban_map(wb: xl.Book, account_ids: list[str]) -> dict[str, str]:
    name_iban = wb.sheets["Comptes"].range("C2:E50").value
    map = {}
    for account_id in account_ids:
        account = get_account_info(account_id)
        map[account["iban"]] = next(
            row[0]
            for row in name_iban
            if (row[2] is not None and row[2] == account["iban"])
        )
    return map


def get_col_values(sheet: xl.Sheet, col_name: str) -> list:
    return sheet.range(f"{col_name}:{col_name}")[1:MAX_ROWS].value


def find_last_row(sheet: xl.Sheet, col_name: str) -> int:
    return (
        next(
            row
            for row, value in enumerate(get_col_values(sheet, col_name))
            if value == None
        )
        + 1
    )


def write_rows(sheet: xl.Sheet, start_row: int, start_col: int, data: list[list]):
    overwrite_area = sheet[
        start_row : start_row + len(data), start_col : start_col + len(data[0])
    ].value
    if type(overwrite_area[0]) is not list:
        overwrite_area = [overwrite_area]
    if any([cell for row in overwrite_area for cell in row]):
        raise Exception("Cannot write over existing data")
    else:
        print(f"Writing {len(data)} rows starting at ({start_col},{start_row})")
        if len(data) > 0:
            sheet[start_row, start_col].value = data


def filter_duplicate_keys(sheet: xl.Sheet, key_col: str, key: str, data: list[list]):
    source_keys = get_col_values(sheet, key_col)
    for row in data[:]:
        if row[key] in source_keys:
            data.remove(row)


def get_account_balance_iban_map(
    wb: xl.Book, account_ids: list[str]
) -> dict[str, float]:
    balance_iban = wb.sheets["Comptes"].range("E2:I50").value
    map = {}
    for account_id in account_ids:
        account = get_account_info(account_id)
        map[account["iban"]] = next(
            float(row[4])
            for row in balance_iban
            if (row[0] is not None and row[0] == account["iban"])
        )
    return map


# Creates a backup file before opening the spreadsheet
def open_workbook(path: str) -> xl.Book:
    try:
        copy(path, f"{path}.bak")
        return xl.Book(path)
    except PermissionError:
        raise PermissionError("Please close the spreadsheet before running the script")


def close_workbook(wb: xl.Book, save: bool = False):
    if save:
        wb.save()
    wb.close()
    app = xl.apps.active
    if app:
        app.quit()


if __name__ == "__main__":
    path = sys.argv[1] if len(sys.argv) > 1 else SPREADSHEET_PATH
    try:
        wb = open_workbook(path)
        # print(get_account_name_iban_map(wb, get_all_accounts()))
        # print(find_last_row(wb.sheets["Opérations"], "A"))
        # write_transactions(wb, get_transaction_rows(get_all_accounts()[1]))
        write_all_transactions(wb)
        close_workbook(wb, True)
    except PermissionError as e:
        print(e)
    except Exception as e:
        print(e)
        close_workbook(wb)
